#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SoftSerialSTM32.h>



//SoftSerialSTM32 gpsSerial(PA11, PA12); // RX, TX

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET); //initializing the display Object for oled


char longic[12],latic[12];
//global Variables for Hr Sensor

int UpperThreshold = 540;
int LowerThreshold = 530;
int PulseIn = 5;
int reading = 0;
float BPM = 0.0;
bool IgnoreReading = false;
bool FirstPulseDetected = false;
bool BPMinvalid=false;
unsigned long FirstPulseTime = 0;
unsigned long SecondPulseTime = 0;
unsigned long PulseInterval = 0;
const unsigned long delayTime = 10;
const unsigned long delayTime2 = 10;
const unsigned long baudRate = 115200;
unsigned long previousMillis = 0;
unsigned long previousMillis2 = 0;
unsigned long timeDispMilli=0,last_gpsMillis=0;
//variables for gps module

String inputString = ""; // a string to hold incoming data
boolean stringComplete = false; // whether the string is complete
String Signal = "$GPGLL";
String minutes = "";
float loni,lati;
//variables for problem_detection part
const String emergency_Contacts[5]  = {"8496816208","8861643120","9631777402","9686621569","8884700999"};
bool In_Problem = false;
bool problem_timer = false; // to check if the problem timer has started r not.
unsigned long problem_start_time=0,In_Problem_Timer=0;
int average_hrtrate = 80;
bool problem_handled = false;

//variables for updating live values
unsigned long lastUpdateTime = 0;
bool data_sent = false;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);//for Sserial Monitor
  Serial1.begin(115200);//for gsmModule
  Serial2.begin(9600); //for gps
  Wire.setClock(400000); 
  inputString.reserve(200); // reserve space for 200 character string
  
  Setup_hrsensor();
  
  Serial1.println("AT+CMEE=2");
  delay(1000);
  start_gprs();
  //delay(5000);
  Setup_display();
  delay(1000);
  Serial1.print("AT+HTTPINIT\r");
  //Send_Data("autolligentbeta.000webhostapp.com/submitmail.php?name=satyajeet&email=satyajeet.ra2016@gmail.com&message=Started");
  delay(2000);
  //stop_gprs();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(millis()-lastUpdateTime>5000)
  {
    String bpm="";
    if(BPMinvalid){
      bpm = "0";
      }
      else{
        bpm = (String)BPM;
      }
    int prob=0;
    if(In_Problem)
    prob=1;
    else
    prob =0;
    String url = "http://project8thsem.000webhostapp.com/u.php?hrt="+bpm+"&lati="+latic+"&longi="+longic+"&prob="+prob;
//    Serial.println(url);
    Send_Data(url);
    lastUpdateTime = millis();
  }
  if(millis()-lastUpdateTime>1000){
    if(data_sent){
    Serial1.println("AT+HTTPACTION=0\r");
    data_sent = false;
    }
    }
  if(Serial2.available()){
  Serial2.readBytes(longic,10);
//   Serial.println(longic);
   
  Serial2.readBytes(latic,10);
//   Serial.println(latic);
  Serial2.end();
 
  Serial2.begin(9600);
  }
  if(In_Problem)
  {
    if(millis()-In_Problem_Timer>10000){
    //problem_start_time = millis();
    if(!problem_handled){
      Serial.println("Handling");
    Handle_problem();
    }
   //else
//    Serial.println("Problem was Handled Once\n");
    //Serial.println("problem Detected");
    }
     if(problem_timer)
     {
//      display.clearDisplay();
      String msgd = "Reporting the problem in 10 seconds\n"+(String)((millis()-In_Problem_Timer)/1000);
      display_message(10,10,msgd,1,false,false);
      display.display();
     }


    
  }
  
  

  

  loop_hrsensor();

//
// if(Serial1.available()){
//  Serial.println(Serial1.readString());
//
//    Serial.flush();
//
//
// }
 if((char)Serial.read() == 'c'){
 Serial.println("Stopping");
  In_Problem = false;
  In_Problem_Timer = 0;
  problem_handled = false;
  problem_timer =  false;
 }
// if(Serial.available()){
//  Serial1.println(Serial.readString());
//  Serial.flush();
// }

// if(Serial2.available()){
//  Serial.print((char)Serial2.read());
//  Serial.flush();
// }
}


void Handle_problem()
{
  problem_handled = true;
   Serial1.print("AT+CMGF=1\r"); // Set the shield to SMS mode
  delay(100);
  Serial.println("called handler");
    for(int i = 0; i<5;i++)
    {
      String reciever = emergency_Contacts[i];
     
  Serial1.print("AT+CMGS=\"");Serial1.print(reciever);Serial1.print("\"\r");  
  delay(200);
  Serial1.print("http://maps.google.com/?q=");
  Serial1.print(latic);
  Serial1.print(",");
  Serial1.println(longic);
  Serial1.print("MayDay!MayDay!MayDay! Please Help You can track me live here\n");
  
  Serial1.println("http://project8thsem.000webhostapp.com/r.php");
  Serial1.print("\r"); //the content of the message
  delay(500);
  Serial1.print((char)26);
  delay(5000);
      //Send_SMS(reciever,"I am in problem please Help");
//      Serial.println(reciever);
      
    }
  
}
void Set_time()
{
  
  Serial1.println("AT+CCLK?");
  String net_time = Serial1.readString();
  timeDispMilli = millis();
  display_message(10,10,(String)timeDispMilli,1,false,false);
  timeDispMilli = millis();
}
void Setup_display()
{
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  set_size1();
  show_begining();  
  display.display();
  
  
}
void  Setup_hrsensor()
{
  pinMode(PulseIn,INPUT_ANALOG);
  digitalWrite(LED_BUILTIN, LOW);
}

void show_begining()
{
    display.stopscroll();
 display.setTextColor(WHITE);
 display.setCursor(11,0);
 display.setTextSize(1);
 display.println("Developed BY:\n");
 display.setTextSize(1);
 display.println("Satyajeet \t Anitha \t Dhanashree \t Sahana");
 display.startscrollleft(0x00, 0x0F);
 
 display.display();

 delay(5000);
 
 //uncomment the lines below if gsm connected
 Serial1.print("AT+CMGF=1\r"); // Set the shield to SMS mode
  delay(100);
 
  Serial1.print("AT+CMGS=\"8496816208\"\r");  
  delay(200);
//  Serial1.print("http://maps.google.com/?q=");
//  Serial1.print(lat);
//  Serial1.print(",");
//  Serial1.print(lng);
  Serial1.print("Hello from arduino ");
  
 
  Serial1.print("\r"); //the content of the message
  delay(500);
  Serial1.print((char)26);//the ASCII code of the ctrl+z is 26 (required according to the datasheet)
  delay(100);
  Serial1.println();
  Serial.println("Text Sent.");
   delay(500);
   
   
  }
void set_size(){
  display.setTextSize(1);
}

void set_size1(){
  display.setTextSize(2);
}

void loop_hrsensor()
{
   // Get current time
  unsigned long currentMillis = millis();

  // First event
  if(myTimer1(delayTime, currentMillis) == 1){

    reading = analogRead(PulseIn); 
    reading = map(reading,0,4095,0,1023);
    
    

    
    
    // Heart beat leading edge detected.
    if(reading > UpperThreshold && IgnoreReading == false){
      if(FirstPulseDetected == false){
        FirstPulseTime = millis();
        FirstPulseDetected = true;
      }
      else{
        SecondPulseTime = millis();
        PulseInterval = SecondPulseTime - FirstPulseTime;
        FirstPulseTime = SecondPulseTime;
      }
      IgnoreReading = true;
      //digitalWrite(LED_BUILTIN, HIGH);
    }

    // Heart beat trailing edge detected.
    if(reading < LowerThreshold && IgnoreReading == true){
      IgnoreReading = false;
      //digitalWrite(LED_BUILTIN, LOW);
    }  

    // Calculate Beats Per Minute.
    BPM = (1.0/PulseInterval) * 60.0 * 1000;
  }

  // Second event
  if(myTimer2(delayTime2, currentMillis) == 1){
//    Serial.println(reading);
//    Serial.print("\t");
//    Serial.print(PulseInterval);
//    Serial.print("\t");
    
    if(BPM>220){

      BPMinvalid=true;
    }
    else{
      
      BPMinvalid=false;
      if(BPM>average_hrtrate+10 || BPM<average_hrtrate-13 ){
        if(!problem_timer){
        In_Problem = true;
        In_Problem_Timer = millis();
        problem_timer = true;
        }
      }
      else{
        if(millis()-In_Problem_Timer>10000){
          {
            In_Problem = false;
          }
        }
      }
    }
  //  Serial.println(BPM);
    display.clearDisplay();
    display.setCursor(30,40);
    display.setTextSize(2);
    display.stopscroll();
   Show_Bpm(BPM);
   display.display();
//    Serial.println(" BPM ");
   
    
    Serial.flush();
    
  }

}
// First event timer
int myTimer1(long delayTime, long currentMillis){
  if(currentMillis - previousMillis >= delayTime){previousMillis = currentMillis;return 1;}
  else{return 0;}
}

// Second event timer
int myTimer2(long delayTime2, long currentMillis){
  if(currentMillis - previousMillis2 >= delayTime2){previousMillis2 = currentMillis;return 1;}
  else{return 0;}
}


void Show_Bpm(int beats)
{
  if(!BPMinvalid)
  display.println(beats);
  else
  display.println("Invalid HeartRate");
}
void display_message(int x,int y,String msg,int set_size,bool clear_disp,bool scroll_left)
{
  if(clear_disp){
    display.clearDisplay();
    }
  display.stopscroll();
  display.setTextColor(WHITE);
  display.setCursor(x,y);
  display.setTextSize(set_size);
  display.println(msg);
  if(scroll_left){
    display.startscrollleft(0x00, 0x0F);
  }
  //display.display();

  
}
void Send_Data(String url)
{
//  delay(1000);
  
  
  if(!data_sent){
  Serial1.print("AT+HTTPPARA=\"url\",");  Serial1.print("\"");  Serial1.print(url);  Serial1.println("\"\r");
  data_sent = true;
  }
//  Serial.println(url);
//  Serial.println("calledoff");
  
  
}

void start_gprs(){
  Serial1.println("AT+SAPBR=1,1\r");

}
void stop_gprs(){
  Serial.println("AT+SAPBR=0,1\r");
  
  }


void get_gpsLoc(){
  if (Serial2.available()) {
        // get the new byte:
        char inChar = (char) Serial2.read();
        // add it to the inputString:
        inputString += inChar;
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it:
        if (inChar == '\n') {
            stringComplete = true;
        }
    }
    else{
      Serial.println("NO connection to gps");
    }
    // print the string when a newline arrives:
    if (stringComplete) {
        String BB = inputString.substring(0, 6);
        if (BB == Signal) {
            String LAT = inputString.substring(7, 17);
            int LATperiod = LAT.indexOf('.');
            int LATzero = LAT.indexOf('0');
            if (LATzero == 0) {
                LAT = LAT.substring(1);
            }
            String LATI = LAT.substring(0,2);
            minutes = LAT.substring(2);           
            lati = get_cord(LATI,minutes);
            
            
            String LON = inputString.substring(20, 31);
            int LONperiod = LON.indexOf('.');
            int LONTzero = LON.indexOf('0');
            if (LONTzero == 0) {
                LON = LON.substring(1);
            }
            String LONGI = LON.substring(0,2);
            minutes = LON.substring(2);           
            loni = get_cord(LONGI,minutes);
            
          
           Serial.println(lati);
           Serial.println(loni);
           
            
            
            

        }

        // Serial.println(inputString);
        // clear the string:
        inputString = "";
        stringComplete = false;
    }
}
float get_cord(String Lat,String Minutes)
{
    float temp = Minutes.toFloat()/60;
    float coord = Lat.toFloat()+temp;
    return coord;
}


void Send_SMS(String mob_num,String message)
{
  Serial1.print("AT+CMGF=1\r"); // Set the shield to SMS mode
  delay(100);
  Serial1.print("AT+CMGS=\"");Serial1.print(mob_num);Serial1.print("\"\r");  
  delay(200);
  Serial1.print("http://maps.google.com/?q=");
  Serial1.print(lati);
  Serial1.print(",");
  Serial1.println(loni);
  Serial1.print(message);
  Serial1.print("\r"); //the content of the message
  delay(500);
  Serial1.print((char)26);//the ASCII code of the ctrl+z is 26 (required according to the datasheet)
  delay(3000);
  //Serial1.println();
}

