<html lang="en">
<head>
<title>Smart Assistant</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
body {
	width:100%;	
	height:100%;
	
    font-family: Arial, Helvetica, sans-serif;
    margin: 0;
}
#blankLeft{
	
	
	float:left;
}
#mainContainer{
		height:100%;
}
#blankRight{
	
	width:5%;
	
	float:right;
}
* {
    box-sizing: border-box;
}
#map {
        height: 400px;
      }


.header {
    padding: 80px;
    text-align: center;
    background: #1abc9c;
    color: white;
}


.header h1 {
    font-size: 40px;
}
.navbar {
    overflow: hidden;
    background-color: #333;
}


.navbar a {
    float: left;
    display: block;
    color: white;
    text-align: center;
    padding: 14px 20px;
    text-decoration: none;
}


.navbar a.right {
    float: right;
}


.navbar a:hover {
    background-color: #ddd;
    color: black;
}
#selector{
	
	
	

	
}
#selector input{
	margin-top:20px;
	margin-right:10px;
	margin-bottom:2%;
	margin-left:2%;
}
</style>
 
</head>
<body> 
<div id="blankLeft"></div>
<div id="mainContainer">
<h1 align="center" id="headerMain">Displaying all the data on the map</h1>
<?php require_once("header.php");?>

 <div id="map"></div>
 
 <div id="selector">
  	<h1  align="center" style="color:white;background-color:darkmagenta"> Please select a Date range and click Go!</h1>
 	
 	<b style="margin-left:10px">From</b><input type="date" id="dateFrom" >
 	<b>TO</b><input type="date" id="dateTo">
 	<input type="button" onclick="dateFunc()" value="Go!" class="btn-success">
 </div>
 
 </div>
 <div id="blankRight"></div>
  
  <script>
  var dateFrom="";
  var dateTo="";
   function dateFunc(){
   var from = document.getElementById('dateFrom').value;
   var to = document.getElementById('dateTo').value;
   dateFrom = getDatefinal(from);
   dateTo = getDatefinal(to);
   document.getElementById('headerMain').innerHTML="Displaying data From "+dateFrom+" To "+dateTo;
   var uri = "/rall.php?param=2&from="+dateFrom+"&to="+dateTo;
   deleteMarkers();
   loadDoc(uri,deliverResponse);
   setTimeout(setheader,1000);
   
   
   }
   function setheader()
   {
   document.getElementById('headerMain').innerHTML="Displaying data From "+dateFrom+" To "+dateTo+" ("+Markers.length+" Location entries. )";
   document.documentElement.scrollTop = 0;
   document.body.scrollTop = 0;
   
   }
   function getDatefinal(data)
   {
     var year = data.slice(2,4);
     var month = data.slice(5,7);
     var day = data.slice(8,10);
     var date = day + "-"+month+ "-"+year;
   return date;
   }
   
    
   
  </script>


 <script>
var map ;
var Markers = [];

function initMap() 
{
  // initialized map
  //alert("initialized");
  var initLoc = {lat: 13.065454 , lng:77.6545};
   
   map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center:initLoc});
  // The marker, positioned at Uluru
  //var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
       
      <script 
      async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARcff3S_i4Pxc_HMf8GBjrmCLU0FBsNWA&callback=initMap">
      
    </script>
    <script>
    var response,date,time,latitude,longitude,heartRate,problem;
    var marker;
    
    
    
    
    function helperFunc()
    {
    
    loadDoc("/rall.php?param=1",deliverResponse);
    //marker = null;
    //setTimeout(helperFunc,6000);
    }
     function deliverResponse()
  {
        
    	
  		var dataArray = new Array();
  		
  		dataArray = response.split("%");
  		dataArray.forEach(finalMapData);
  	}
  	function finalMapData(item)
  	{
  		temparray = item.split(",");
  		date = temparray[0];
  		time = temparray[1];
  		heartRate = temparray[2];
  		latitude = temparray[3];
  		longitude = temparray[4];
  		problem = temparray[5];
  		var Title = date +" "+ time;
		
  		var latLng = new google.maps.LatLng(latitude,longitude);
  		marker = new google.maps.Marker(
  						{position: latLng, 
  						map: map,
  						title: Title,
  						label: time,
  						animation: google.maps.Animation.DROP
  						}
  						);
  		Markers.push(marker);
  		
  		
  	}
  						
  	function setMapOnAll(map)
  	 {
        for (var i = 0; i < Markers.length; i++) {
          Markers[i].setMap(map);
        }
      }	

  	 function clearMarkers()
  	  {
        setMapOnAll(null);
      }
  	  function deleteMarkers() {
        clearMarkers();
        Markers = [];
      }
  	
       
    function loadDoc(url, cFunction) 
    {
    
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.onreadystatechange = function()
   {
    if (this.readyState == 4 && this.status == 200) 
    	{
        	response = this.responseText;
    	
      cFunction(this);
    	}
    };
  xhttp.open("GET", url, true);
  xhttp.send();
  
   }
  helperFunc();
 
 
  </script>






</body>

</html>
