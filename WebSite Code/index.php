<?php
require_once("r.php");
global $latitude;
global $longitude;
?>





<!DOCTYPE html>
<html lang="en">
<head>
<title>Smart Assistant</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	width:100%;	
	height:100%;
    font-family: Arial, Helvetica, sans-serif;
    margin: 0;
}



* {
    box-sizing: border-box;
}
#map {
        height: 400px;
      }


.header {
    padding: 80px;
    text-align: center;
    background: #1abc9c;
    color: white;
}


.header h1 {
    font-size: 40px;
}
.navbar {
    overflow: hidden;
    background-color: #333;
}


.navbar a {
    float: left;
    display: block;
    color: white;
    text-align: center;
    padding: 14px 20px;
    text-decoration: none;
}


.navbar a.right {
    float: right;
}


.navbar a:hover {
    background-color: #ddd;
    color: black;
}
</style>
 
</head>
<body>
<h1 align="center">Smart Assistant</h1> 
<?php require_once("header.php");?>
 <div id="map"></div>
  <audio id="myAudio" style="display:none;">
    <source src="../media/audio/beep_problem.mp3" type="audio/mpeg">
  </audio>
  <span style="margin-top:5%;">
  <img src="../media/images/heartRed.png" width="40px" height="55px" alt="Read Heart image" style="margin-left:20%;"/><b style="margin-left:10px;color:red" id="heartrateText">
  Heart rate </b>
  </span>
    
   <script>
// Initialize and add the map
//var ulurus = {lat: <?php echo $latitude?>, lng: <?php echo $longitude?>};
var map ;
var Markers = [];

function initMap() 
{
  // initialized map
  //alert("initialized");
  var initLoc = {lat: 13.065454 , lng:77.6545};
   
   map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center:initLoc});
  // The marker, positioned at Uluru
  //var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
       
      <script 
      async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARcff3S_i4Pxc_HMf8GBjrmCLU0FBsNWA&callback=initMap">
      
    </script>
    <script>
    var response,date,time,latitude,longitude,heartRate,problem;
    var marker;
    
    
    
    
    function helperFunc()
    {
    deleteMarkers();
    loadDoc("/raw.php",deliverResponse);
    
    setTimeout(helperFunc,3000);
    }
     function deliverResponse()
  {
        
    	
  		var temparray = new Array();
  		temparray = response.split(",");
  		date = temparray[0];
  		time = temparray[1];
  		heartRate = temparray[2];
  		latitude = temparray[3];
  		longitude = temparray[4];
  		problem = temparray[5];
  		document.getElementById('heartrateText').innerHTML = heartRate + " BPM";
  		if(problem ==1){
  		document.getElementById('myAudio').play();
  		
  		}
  		else{
  		document.getElementById('myAudio').pause();
  		}
  		var Title = date +" "+ time;
		
  		var latLng = new google.maps.LatLng(latitude,longitude);
  		marker = new google.maps.Marker(
  						{position: latLng, 
  						map: map,
  						title: Title,
  						label: time,
  						animation: google.maps.Animation.BOUNCE
  						}
  						);
  		Markers.push(marker);
  						
  		

  	   	  
  	   	 
  	   	  
  	 
  }
  function setMapOnAll(map)
  	 {
        for (var i = 0; i < Markers.length; i++) {
          Markers[i].setMap(map);
        }
      }	

  	 function clearMarkers()
  	  {
        setMapOnAll(null);
      }
  	  function deleteMarkers() {
        clearMarkers();
        Markers = [];
      }
  	
       
    function loadDoc(url, cFunction) 
    {
    
  var xhttp;
  xhttp=new XMLHttpRequest();
  xhttp.onreadystatechange = function()
   {
    if (this.readyState == 4 && this.status == 200) 
    	{
        	response = this.responseText;
    	
      cFunction(this);
    	}
    };
  xhttp.open("GET", url, true);
  xhttp.send();
  
   }
  helperFunc();
 
 
  </script>


  </body>
</html>

