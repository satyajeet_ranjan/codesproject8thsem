<?php
require_once("connection.php");
global $conn;
?>


<!DOCTYPE html>
<html>

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<title>Showing all data</title>
<style>
body {
	width:100%;	
	height:100%;
	
    font-family: Arial, Helvetica, sans-serif;
    margin: 0;
}
#blankLeft{
	
	
	float:left;
}
#mainContainer{
		height:100%;
}
#blankRight{
	
	width:5%;
	
	float:right;
}
* {
    box-sizing: border-box;
}
#map {
        height: 400px;
      }


.header {
    padding: 80px;
    text-align: center;
    background: #1abc9c;
    color: white;
}


.header h1 {
    font-size: 40px;
}
.navbar {
    overflow: hidden;
    background-color: #333;
}


.navbar a {
    float: left;
    display: block;
    color: white;
    text-align: center;
    padding: 14px 20px;
    text-decoration: none;
}


.navbar a.right {
    float: right;
}


.navbar a:hover {
    background-color: #ddd;
    color: black;
}
#selector{
	
	
	

	
}
#selector input{
	margin-top:20px;
	margin-right:10px;
	margin-bottom:2%;
	margin-left:2%;
}
</style>

</head>

<body>
<h1 align="center"> Showing all the data in the data base</h1>
<?php require_once("header.php");?>
<table class="table table-striped"style="width:100%; padding:0 2% 0 2%;">
<thead class="thead-dark"><tr style="color:indigo"><th>Heart rate</th><th>latitude</th><th>longitude</th><th>date</th><th>Time</th><th>location on map</th></tr></thead>
<?php 
$retrive_querry = "SELECT * FROM `user_details`;";

$result =  $conn->query($retrive_querry);

if($result)
{
	
	while($row = $result->fetch_assoc()){

	$heartRate = $row['heartrate'];
	$latitude = $row['lati'];
	$longitude = $row['longi'];
	$problem = $row['problem'];
	$date = $row['date'];
	$time = $row['time'];
	$problem = (int)$problem;
	$longitude = str_replace(" ","",$longitude);
	$latitude = str_replace(" ","",$latitude);
 echo "<tr><td>".$heartRate."</td>"."<td>".$latitude."</td>"."<td>".$longitude."</td>"."<td>".$date."</td>"."<td>".$time."</td>"."<td><a href=https://maps.google.co.in?q=".$latitude.",".$longitude.">Click here</a></td></tr>";
		
	}
}
?>
</table>
</body>

</html>
