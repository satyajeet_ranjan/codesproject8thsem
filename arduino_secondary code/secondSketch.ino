#include <MPU6050_tockn.h>
#include <Wire.h>
#include<SoftwareSerial.h>

char latic[10],longic[10];
SoftwareSerial gps(8,9);
SoftwareSerial SerialDelivery(5,6);
String inputString = ""; // a string to hold incoming data
boolean stringComplete = false; // whether the string is complete
String signal = "$GPGLL";
String minutes = "";
float loni,lati;

MPU6050 mpu6050(Wire);

long timer = 0;

void setup() {
  Serial.begin(9600);
//  Wire.begin(8);
  
//  mpu6050.begin();
//  mpu6050.calcGyroOffsets(true);
   gps.begin(9600);
    // reserve 200 bytes for the inputString:
    inputString.reserve(200);

  pinMode(5,OUTPUT);
  pinMode(13,OUTPUT);
}

void loop() {
  get_gpsLoc();
  
// mpu6050.update();
// Serial.println(lati);
// Serial.println(loni);
if(millis() - timer > 3000){
//    
//    //Serial.println("=======================================================");
//   // Serial.print("temp : ");Serial.println(mpu6050.getTemp());
//
//    float temp = mpu6050.getTemp();
//    float x = mpu6050.getAngleX();
    //Serial.println(x);
//    if(x>80){
//      Serial.println("Problem");
//      digitalWrite(13,LOW);
//      
//    }
//    else{
//      digitalWrite(13,1);
//    }
////    Serial.print("angleX : ");Serial.print(mpu6050.getAngleX());
////    Serial.print("\tangleY : ");Serial.print(mpu6050.getAngleY());
////    Serial.print("\tangleZ : ");Serial.println(mpu6050.getAngleZ());
////    Serial.println("=======================================================\n");
//Serial.println(lati);
//Serial.println(loni);
//

//  Wire.beginTransmission(


    timer = millis();
//    
  }

}
void get_gpsLoc()
{
  while(gps.available()) {
        // get the new byte:
        char inChar = (char) gps.read();
        // add it to the inputString:
        inputString += inChar;
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it:
        if (inChar == '\n') {
            stringComplete = true;
            
        }
    }
    // print the string when a newline arrives:
    if (stringComplete) {
        String BB = inputString.substring(0, 6);
        //Serial.println(BB);
        if (BB == signal) {
            String LAT = inputString.substring(7, 17);
            int LATperiod = LAT.indexOf('.');
            int LATzero = LAT.indexOf('0');
            if (LATzero == 0) {
                LAT = LAT.substring(1);
            }
            String LATI = LAT.substring(0,2);
            //Serial.println(LAT);
            minutes = LAT.substring(2);           
            lati = get_cord(LATI,minutes);
            
            
            String LON = inputString.substring(20, 31);
            int LONperiod = LON.indexOf('.');
            int LONTzero = LON.indexOf('0');
            if (LONTzero == 0) {
                LON = LON.substring(1);
            }
            String LONGI = LON.substring(0,2);
            minutes = LON.substring(2);           
            loni = get_cord(LONGI,minutes);

//            Serial.println(LONGI);
//            Serial.println(minutes);
            
          
//           Serial.println(lati,8);
//           Serial.println(loni,8);
           String messLoc = (String)(lati)+","+(String)(loni);
           SerialDelivery.write(longic,10);
           dtostrf(lati,11,8,latic);
           
//           Serial.print("lati");
//           Serial.println(latic);
           dtostrf(loni,11,8,longic);
//           Serial.print("longi");

           Serial.write(longic,10);
           Serial.write(latic,10);
//         
//           
            
//            Serial.println(inputString);
              
              
           
           

        }

//         Serial.println(inputString);
        // clear the string:
        inputString = "";
        stringComplete = false;
        //mpu6050.resetBuffer();
        //mpu6050.update();
    }
}
/*
SerialEvent occurs whenever a new data comes in the
hardware serial RX. This routine is run between each
time loop() runs, so using delay inside loop can delay
response. Multiple bytes of data may be available.
*/
float get_cord(String Lat,String Minutes) {
    float temp = Minutes.toFloat()/60;
    float coord = Lat.toFloat()+temp;
    return coord;
}
//void serialEvent() {
//  while (Serial.available()) {
//    // get the new byte:
//    char inChar = (char)Serial.read();
//    // add it to the inputString:
//    inputString += inChar;
//    // if the incoming character is a newline, set a flag so the main loop can
//    // do something about it:
//    if (inChar == '\n') {
//      stringComplete = true;
//    }
//  }
//}
