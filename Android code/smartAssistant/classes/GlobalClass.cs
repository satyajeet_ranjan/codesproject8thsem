﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace smartAssistant
{
    public class GlobalClass
    {
        public static string hrtRate
        {
            get;set;
        }
                    
        public static string date
        {
            get; set;
        }

        public static string latitude
        {
            get; set;
        }
        public static string longitude
        {
            get; set;
        }
        public static string time
        {
            get; set;
        }
        public static int problem
        {
            get; set;
        }
    }
}