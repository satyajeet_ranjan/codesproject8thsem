﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace smartAssistant
{
    class UtilityClass
    {
        MainActivity mainobj;
        public UtilityClass()
        {
            mainobj = new MainActivity();
        }
        public void WebUtility(String url)
        {
            Uri FinalUrl = new Uri(url);
            WebClient clientWeb = new WebClient();
            clientWeb.DownloadStringAsync(FinalUrl);
            clientWeb.DownloadStringCompleted += ClientWeb_DownloadStringCompleted;
        }

        private void ClientWeb_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                String data = e.Result;
                String[] Finaldata = data.Split(new String[] { "," }, StringSplitOptions.None);
                String date = Finaldata[0];
                String time = Finaldata[1];
                String HeartRate = Finaldata[2];
                String latitude = Finaldata[3];
                String longitude = Finaldata[4];
                String problem = Finaldata[5];
                int prob = int.Parse(problem);
                GlobalClass.hrtRate = HeartRate;
                GlobalClass.latitude = latitude;
                GlobalClass.longitude = longitude;
                GlobalClass.time = time;
                GlobalClass.date = date;
                GlobalClass.problem = prob;
            }
            catch (Exception ex)
            {
               
            }
        }
    }
}