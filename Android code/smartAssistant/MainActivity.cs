﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Timers;
using Android.Content;
using smartAssistant.services;
using Java.Lang;
using Android.Icu.Util;
using Android.Views;
using Android.Graphics;
using Android.Net;
using Android.Media;
using Android.Views.Animations;
using Android.Support.V7.App;
using Android.Support.V4.Content;
using Android.Content.PM;
using Android.Support.V4.App;
using static Android.Support.V4.App.ActivityCompat;
using Android.Runtime;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

namespace smartAssistant
{
    [Activity(Label = "smartAssistant", MainLauncher = true,Theme ="@style/Theme.AppCompat.Light")]
    public class MainActivity : AppCompatActivity, IOnRequestPermissionsResultCallback,IOnMapReadyCallback
    {
        private GoogleMap map;
        private TextView hrtrateview;
        private ImageView heartImg;
        private Button cancelButton;
        private Bitmap hrtBitmap;
        private Timer timer;
        private int count = 1;
        private bool timerRunning = false, paused = false;
        LinearLayout mainView;
        UtilityClass UtilityObj;
        Animation blinkAnim,zoomInAnim,zoomOutAnim;
        private bool canceled=false,zoomout=false;
        Uri ringnotification;
        Ringtone alarm;
        Notification.Builder notificationBuilder;
        PendingIntent pintent;
        private bool has_permission = false;
        NotificationManager notificationManager;
        private ToggleButton mapToggle;
        Intent activityIntent;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            //tv = FindViewById<TextView>(Resource.Id.newTextView);
            hrtrateview = FindViewById<TextView>(Resource.Id.hrtrateView);
            //dateview = FindViewById<TextView>(Resource.Id.DateView);
            //latitudeview = FindViewById<TextView>(Resource.Id.latView);
            mainView = FindViewById<LinearLayout>(Resource.Id.mainView);
            heartImg = FindViewById<ImageView>(Resource.Id.heartImg);
            hrtBitmap = BitmapFactory.DecodeResource(Resources, Resource.Drawable.heartRed);
            heartImg.SetImageBitmap(hrtBitmap);
            blinkAnim = AnimationUtils.LoadAnimation(this,Resource.Animation.blink);
            zoomInAnim = AnimationUtils.LoadAnimation(this, Resource.Animation.zoominout);
            zoomOutAnim = AnimationUtils.LoadAnimation(this, Resource.Animation.zoomout);
            notificationBuilder = new Notification.Builder(this);
            heartImg.Animation = blinkAnim;
            hrtrateview.Animation = blinkAnim;
            hrtrateview.SetTextSize(Android.Util.ComplexUnitType.Dip,20);
            hrtrateview.SetTextColor(Color.Red);
            UtilityObj = new UtilityClass();
            cancelButton.Click += CancelButton_Click;
            mapToggle = FindViewById<ToggleButton>(Resource.Id.mapViewtoggle);
            mapToggle.Click += MapToggle_Click;
            activityIntent = new Intent(this, typeof(MainActivity));
            pintent = PendingIntent.GetActivity(this, 0, activityIntent, PendingIntentFlags.OneShot);
            //Notification notification = notificationBuilder.Build();
            notificationManager = GetSystemService(Context.NotificationService) as NotificationManager;
            initialize_map();
        }

        private void MapToggle_Click(object sender, System.EventArgs e)
        {
            if (map != null)
            {
                if (!mapToggle.Checked)
                {
                    map.MapType = GoogleMap.MapTypeTerrain;
                }
                else
                {
                    map.MapType = GoogleMap.MapTypeSatellite;
                }
            }
        }

        public void initialize_map()
        {
            if (map == null)
            {
                FragmentManager.FindFragmentById<MapFragment>(Resource.Id.mapFragment).GetMapAsync(this);
            }
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            if (canceled)
            {
                //resume clicked
                cancelButton.Text = "Cancel";
                canceled = false;
                check_timer();
                heartImg.StartAnimation(blinkAnim);
                hrtrateview.StartAnimation(zoomInAnim);
                zoomout = false;

            }
            else
            {
                //cancel clicked
                cancelButton.Text = "Resume";
                canceled = true;
                check_timer();
                heartImg.ClearAnimation();
                hrtrateview.ClearAnimation();
            }

        }

        protected override void OnPause()
        {
            base.OnPause();


            paused = true;
            if (!canceled)
            {
                notificationBuilder
                .SetContentTitle("Smart Assistant")
                .SetContentText(GlobalClass.hrtRate + " Bpm")
                .SetContentIntent(pintent)
                .SetSmallIcon(Resource.Drawable.heartRed);
                Notification notification = notificationBuilder.Build();

                notificationManager.Notify(0, notification);
            }

        }

        protected override void OnResume()
        {
            base.OnResume();
            //mainView = FindViewById<LinearLayout>(Resource.Layout.Main);
            Check_permission();
            paused = false;
            if (!timerRunning)
            {
                check_timer();
            }
            else
            {
                //Toast.MakeText(this, "Timer running all ready", ToastLength.Short).Show();
                mainView.SetBackgroundColor(Color.Chocolate);
               /* Uri notification = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
                Ringtone r = RingtoneManager.GetRingtone(this, notification);
                r.Play();*/
            }
            ringnotification = RingtoneManager.GetDefaultUri(RingtoneType.Ringtone);
            alarm = RingtoneManager.GetRingtone(this, ringnotification);
        }
        public void check_timer()
        {
            if (timer == null)
            {
                timer = new Timer
                {
                    Interval = 1000
                };
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
                timerRunning = true;
            }
            else
            {
                timer.Stop();
                timer = null;
                timerRunning = false;
            }
        }
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (count < 3)
            {
                count++;
                RunOnUiThread(() =>
                {
                    //tv.Text = count.ToString();
                    hrtrateview.Text = GlobalClass.hrtRate+" BPM";
                    if (GlobalClass.problem==1)
                    {
                        if(!alarm.IsPlaying)
                        alarm.Play();

                       
                    }
                    if (GlobalClass.problem == 0)
                    {
                        if (alarm.IsPlaying)
                            alarm.Stop();


                    }
                    //dateview.Text = GlobalClass.date;
                    if (GlobalClass.latitude != null && GlobalClass.longitude != null)
                    
                        {
                        if (map != null)
                        {
                            try
                            {


                                map.Clear();
                                LatLng pos = new LatLng(double.Parse(GlobalClass.latitude), double.Parse(GlobalClass.longitude));

                                CameraUpdate map_camera = CameraUpdateFactory.NewLatLngZoom(pos, 10);
                                map.MoveCamera(map_camera);
                                MarkerOptions options = new MarkerOptions()
                                .SetPosition(pos)
                                .SetTitle(GlobalClass.date)
                                .SetSnippet("This data arrived at " + GlobalClass.time);

                                map.AddMarker(options);
                            }
                            catch (Exception ex)
                            {
                                //do nothing for any exception caught just let the app run
                            }

                            //latitudeview.Text = GlobalClass.latitude;
                        }
                    }
                });
            }
            else
            {
                count = 1;
                RunOnUiThread(() =>
                {
                    //Toast.MakeText(this, "Restarting", ToastLength.Short).Show();
                    mainView.SetBackgroundColor(Color.WhiteSmoke);
                    if (paused)
                    {
                        notificationBuilder
               .SetContentTitle("Smart Assistant")
               .SetContentText(GlobalClass.hrtRate + " Bpm")
               .SetContentIntent(pintent)
               .SetSmallIcon(Resource.Drawable.heartRed)
               .SetColor(Color.Red);
                        Notification notification = notificationBuilder.Build();

                        notificationManager.Notify(0, notification);
                    }

                });
                UtilityObj.WebUtility("http://project8thsem.000webhostapp.com/raw.php");
            }
            int temp = int.Parse(GlobalClass.hrtRate);
            float animationDuration = 60000 / temp;
            zoomOutAnim.Duration = (long)(animationDuration/2);
            zoomInAnim.Duration = (long)animationDuration/2;
            if (!canceled)
            {
                if (zoomout)
                {

                    hrtrateview.StartAnimation(zoomInAnim);

                    zoomout = false;
                }
                else
                {
                    hrtrateview.StartAnimation(zoomOutAnim);
                    zoomout = true;
                }
            }
        }

        #region checkPermission
        public void Check_permission()
        {

            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.AccessFineLocation) != (int)Permission.Granted)
            {
                has_permission = false;
                if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Android.Manifest.Permission.AccessFineLocation))
                {
                    ActivityCompat.RequestPermissions(this, new string[] { "android.permission.ACCESS_FINE_LOCATION" }, 1000);
                }
                else
                {
                    ActivityCompat.RequestPermissions(this, new string[] { "android.permission.ACCESS_FINE_LOCATION" }, 1000);
                }
            }
            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
            {
                has_permission = false;
                if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Android.Manifest.Permission.WriteExternalStorage))
                {
                    ActivityCompat.RequestPermissions(this, new string[] { "android.permission.WRITE_EXTERNAL_STORAGE" },1000);
                }
                else
                {
                    ActivityCompat.RequestPermissions(this, new string[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 1000);
                }
            }
            else
            {
                has_permission = true;
            }




        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {

            if (requestCode == 1)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    has_permission = true;
                }
            }

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            map = googleMap;
        }
        #endregion



    }
}

